"use strict";

const path = require('path');
// Modules npm
require('dotenv').config({ path: path.join(__dirname, "./env/" + process.env.NODE_ENV + ".env") });
var http = require('http');
const PORT = process.env.PORT;

const app = require('./app.js');

// Lancement du serveur
http.createServer(app).listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});