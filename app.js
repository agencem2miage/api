"use strict";

const routes = require('./app/routes');
const services = require('./app/services');

// Modules npm
const helmet = require("helmet");
const express = require("express");
var compression = require('compression')
const app = express();

/**
 * Sécurité
 */
app.use(helmet.dnsPrefetchControl());
app.use(helmet.expectCt());
app.use(helmet.frameguard());
app.use(helmet.hidePoweredBy());
app.use(helmet.hsts());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.permittedCrossDomainPolicies());
app.use(helmet.referrerPolicy());
app.use(helmet.xssFilter())

app.use(services.security.apiLimiter);

/**
 * Définition des CORS
 */
app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, x-auth-token');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
	next();
});


/**
 * Définition des limites de poids de requêtes
 */
app.use(compression())
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

app.use('/images', express.static('./images'));
app.use('/document-location', express.static('./document-location'));

app.use('/api/users', routes.users);
app.use('/api/bien', routes.bien);
app.use('/api/images', routes.images);
app.use('/api/file', routes.file);
app.use('/api/bail', routes.bail);
app.use('/api/connexion', routes.auth);
app.use('/api/register', routes.register);
app.use('/api/address', routes.address);
app.use('/api/pieces', routes.pieces);

if (process.env.NODE_ENV === 'production') {
	var pathFront = __dirname + "/dist/front-office/";
	var pathBack = __dirname + "/dist/back-office/";

	app.use('/back', express.static(pathBack));
	app.use("/back", (req, res) => {
		res.sendFile(pathBack + "index.html");
	});

	app.use('/', express.static(pathFront));
	app.use("/", (req, res) => {
		res.sendFile(pathFront + "index.html");
	});
}

module.exports = app;
