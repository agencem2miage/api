/*
 Navicat Premium Data Transfer

 Source Server         : Bénélove
 Source Server Type    : MySQL
 Source Server Version : 100410
 Source Host           : localhost:3306
 Source Schema         : agence_zemmari

 Target Server Type    : MySQL
 Target Server Version : 100410
 File Encoding         : 65001

 Date: 27/05/2021 16:10:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Records of adresse
-- ----------------------------
BEGIN;
INSERT INTO `adresse` VALUES (1, '21', 'rue du 19 mars 1962', 'résidence Green Way', '33400', 'Talence', 'FRANCE'), (2, '2bis', 'rue nationnal', '', '17250', 'Saint-Porchaire', 'FRANCE'), (3, '571ter', 'avenue de la liberté', 'appartement 113', '33000', 'Marseille', 'FRANCE'), (4, '136', 'rue des pyrénées', '', '17100', 'Saintes', 'FRANCE'), (5, '1024', 'rue de la compagnie créole', '', '75000', 'Paris', 'FRANCE'), (6, '50', 'rue Sainte-Catherine', 'Appt 107', '33000', 'Bordeaux', 'FRANCE'), (7, '25', 'rue de la vente', '', '33000', 'Bordeaux', 'FRANCE');
COMMIT;

-- ----------------------------
-- Records of bail
-- ----------------------------
BEGIN;
INSERT INTO `bail` VALUES (1, '2021-06-10', 1, '2 ans', 'si perte d\'emploi', '1 mois', '520', '40', 1, 1, '560', '560', 'virement', '40', 1, 'mensuel', '20 du mois', '20 du mois', '560', '100', '20', '1100', 1, 0, 0, 'test.pdf', 2, 6, '', '', ''), (2, '2022-07-10', 2, '', '', '12 mois', '500.52', '30', 0, 1, '500.52', '587.36', 'Forfait charge', '30', 0, '12 mois', 'A terme échu', '15 du mois', '900.52', '0', '0', '300', 1, 1, 1, '/pdf/1621348544856.pdf', 1, 4, '', '', '');
COMMIT;

-- ----------------------------
-- Records of bien
-- ----------------------------
BEGIN;
INSERT INTO `bien` VALUES (1, 560, 2019, 45, 0, 9, 'Cuisine équipée, placard intégrés dans chaque pièces', 'Grande chambre de 15 m², lumineux, parquet flottant', 'Bien neuf, première location dans une résidence calme et moderme à côté des HLM de Talence. Proximité du bus, de toute comodités, de la forêt de thouars\net de Kedge', 'A', 'A', 1, 1, 0, 1, 1, 1, 0, 0, 0, 2, 1, 2, 3, 3, ''), (2, 2090, 2010, 250, 250, 0, 'Cuisine équipée, four, micro ondes et plaques virtocéramiques, barbecue et piscine, placard intégrés dans chaque pièces', 'Grande chambre de 20 m², lumineux, parquet flottant, grands espaces de vies, chambre privée pour la suite parentale avec baignoire', 'Grande maison de ville récente, piscine,\nproche du centre ville de Bordeaux et des magasins. Proximité du bus, de toute comodités, 5 minutes de la gare de Bordeaux et des quais', 'B', 'B', 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 5, 6, ''), (3, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (4, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (5, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (6, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (7, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (8, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (9, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (10, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (11, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (12, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (13, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (14, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, ''), (15, 190000, 1981, 60, 0, 9, 'Cuisine, baignoire', 'Deux petites chambres de 12m², carrelage, grands salon cuisine', 'Vieil appartement en plein centre de Bordeaux à retaper', 'D', 'E', 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 1, 2, 5, 7, '');
COMMIT;

-- ----------------------------
-- Records of duree_bail
-- ----------------------------
BEGIN;
INSERT INTO `duree_bail` VALUES (1, 'Réduite'), (2, '3 ans'), (3, '6 ans');
COMMIT;

-- ----------------------------
-- Records of image
-- ----------------------------
BEGIN;
INSERT INTO `image` VALUES (1, 'chambre_1.jpg', 2), (2, 'toilettes.jpg', 2), (3, 'cuisine.jpg', 2), (4, 'salon.jpg', 2), (5, 'chambre_2.jpg', 1), (6, 'toilettes.jpg', 1), (7, 'maison.jpg', 2), (8, 'appartement.jpg', 1);
COMMIT;

-- ----------------------------
-- Records of majoration
-- ----------------------------
BEGIN;
INSERT INTO `majoration` VALUES (1, 1, 'ajout d\'une piscine', '6 mois', 'Ouvriers du bien du lundi au vendredi', 30000);
COMMIT;

-- ----------------------------
-- Records of piece_bien
-- ----------------------------
BEGIN;
INSERT INTO `piece_bien` VALUES (1, 3, 1), (2, 5, 1), (3, 6, 1), (4, 7, 1), (5, 1, 2), (6, 2, 2), (7, 2, 2), (8, 3, 2), (9, 4, 2), (10, 6, 2), (11, 6, 2), (12, 6, 2), (13, 6, 2), (14, 7, 2), (15, 7, 2), (16, 6, 3), (17, 2, 4), (18, 5, 5), (19, 5, 6), (20, 5, 7), (21, 5, 8), (22, 5, 9), (23, 5, 10), (24, 5, 11), (25, 5, 12), (26, 5, 13), (27, 5, 14), (28, 5, 15);
COMMIT;

-- ----------------------------
-- Records of regime_juridique
-- ----------------------------
BEGIN;
INSERT INTO `regime_juridique` VALUES (1, 'Monopropriete'), (2, 'Copropriété');
COMMIT;

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO `role` VALUES (1, 'Admin'), (2, 'Agent'), (3, 'Propriétaire'), (4, 'Locataire');
COMMIT;

-- ----------------------------
-- Records of travaux
-- ----------------------------
BEGIN;
INSERT INTO `travaux` VALUES (1, 2, 'Changement de fenêtre', 1000);
COMMIT;

-- ----------------------------
-- Records of type_bien
-- ----------------------------
BEGIN;
INSERT INTO `type_bien` VALUES (1, 'Maison'), (2, 'Appartement'), (3, 'Autre');
COMMIT;

-- ----------------------------
-- Records of type_contrat
-- ----------------------------
BEGIN;
INSERT INTO `type_contrat` VALUES (1, 'Location');
COMMIT;

-- ----------------------------
-- Records of type_piece
-- ----------------------------
BEGIN;
INSERT INTO `type_piece` VALUES (1, 'Cuisine'), (2, 'Salle de bain'), (3, 'Salon'), (4, 'Salle à manger'), (5, 'Salle d\'eau'), (6, 'Chambre'), (7, 'Toilettes');
COMMIT;

-- ----------------------------
-- Records of usage_locaux
-- ----------------------------
BEGIN;
INSERT INTO `usage_locaux` VALUES (1, 'Habitation'), (2, 'Mixte');
COMMIT;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES (1, 'Monsieur', 'Alan', 'GRIMARD', 'alan.grimard@agencetourix.fr', '0770341553', '2021-06-10', 'Saintes', '$2b$08$l4cvvMqTMZ/g/sJ8YG8JSOikFHCWUXjkdV8vKBBXLjw9Yptqcx6Fu', 1, 1), (2, 'Autre', 'Julien', 'BASCOU', 'julien.bascou@agencetourix.fr', '0707070707', '2021-06-10', 'Bordeaux', '$2b$08$l4cvvMqTMZ/g/sJ8YG8JSOikFHCWUXjkdV8vKBBXLjw9Yptqcx6Fu', 2, 2), (3, 'Madame', 'Ségolène', 'ROYALE', 'segolene.royale@gmail.fr', '0708080808', '2021-06-10', 'La Rochelle', '$2b$08$l4cvvMqTMZ/g/sJ8YG8JSOikFHCWUXjkdV8vKBBXLjw9Yptqcx6Fu', 3, 3), (4, 'Monsieur', 'Nathan', 'JOUBZ', 'nathan.joubz@gmail.fr', '0709090909', '2021-06-10', 'Triffouillit-les-oies', '$2b$08$l4cvvMqTMZ/g/sJ8YG8JSOikFHCWUXjkdV8vKBBXLjw9Yptqcx6Fu', 4, 4), (5, 'Monsieur', 'Nicolas', 'SARKOZY', 'nico.sarko@gmail.fr', '0710101010', '2021-06-10', 'Paris', '$2b$08$l4cvvMqTMZ/g/sJ8YG8JSOikFHCWUXjkdV8vKBBXLjw9Yptqcx6Fu', 5, 3), (6, 'Monsieur', 'Jean-Vincent', 'PLACEY', 'JeanVincentPlacey@gmail.fr', '0711111111', '2021-06-10', 'Paris', '$2b$08$l4cvvMqTMZ/g/sJ8YG8JSOikFHCWUXjkdV8vKBBXLjw9Yptqcx6Fu', 6, 4);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
