"use strict";

const services = require('../services');

/**
 * Récupère un bien
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou le bien trouvée
 */
const getBien = async (req, res) => {
	let foundBien = [];

	try {
		foundBien = await services.bien.getBien(req.params.bienId, req.userId);
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(foundBien);
};

const getListeBien = async (req, res) => {
	let listeBien = [];

	try {
		listeBien = await services.bien.getListeBien();
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(listeBien);
};

const getListeBienDetail = async (req, res) => {
	let listeBien = [];

	try {
		listeBien = await services.bien.getListeBienDetail();
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(listeBien);
};

const getBienDetail = async (req, res) => {
	let bien = null;

	try {
		bien = await services.bien.getBienDetail(req.params.bienId);
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(bien);
};

const getFilterBien = async (req, res) => {
	let listTypeContrat, listTypeBien, listLocalisation = [];

	try {
		listTypeContrat = await services.bien.getListeTypeContrat();
		listTypeBien = await services.bien.getListeTypeBien();
		listLocalisation = await services.bien.getListeLocalisation();

	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json([{ listTypeContrat: listTypeContrat, listTypeBien: listTypeBien, listLocalisation: listLocalisation }]);
};

const getFilterAdminBien = async (req, res) => {
	let listTypeContrat, listTypeBien, listLocalisation, listProprietaire, listBail = [];
	try {
		listTypeContrat = await services.bien.getListeTypeContrat();
		listTypeBien = await services.bien.getListeTypeBien();
		listLocalisation = await services.bien.getListeLocalisation();
		listProprietaire = await services.bien.getListeProprietaire();
		listBail = await services.bail.getBail();
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json([
		{
			listTypeContrat: listTypeContrat,
			listTypeBien: listTypeBien,
			listLocalisation: listLocalisation,
			listProprietaire: listProprietaire,
			listBail: listBail
		}]);
};



/**
 * Récupère un bien côté Agent Immobilier (bien + propriétaire + bail + locataire...)
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou le bien trouvée
 */
const getBienAgent = async (req, res) => {
	let foundBien = [];

	try {
		foundBien = await services.bien.getBienAgent(req.params.bienId);
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(foundBien);
};

const putEditBien = async (req, res) => {
	let newBien = req.body.newBien;

	try {
		await services.bien.putEditBien(newBien);
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json();
}

/**
 * Ajoute un nouvel utilisateur
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou une confirmation (code 200)
 */
const postCreationBien = async (req, res) => {
	try {
		let newBien = req.body.newBien;
		let idBien;
		if (newBien.image[0] !== req.protocol + '://' + req.get('host') + '/images/undefined') {
			for (let i = 0; i < newBien.image.length; i++) {
				newBien.image[i] = await services.file.saveImage(newBien.image[i]);
			}
		}
		idBien = services.bien.postCreationBien(newBien);

	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json();
}


module.exports = {
	getBien,
	getBienDetail,
	getListeBien,
	getFilterBien,
	getListeBienDetail,
	getFilterAdminBien,
	getBienAgent,
	putEditBien,
	postCreationBien
}