"use strict";

const services = require('../services');

/**
 * Récupère l'utilisateur dont l'id est passé en paramètre de la route
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'utilisateur demandé / une erreur sinon
 */
const updateUser = async (req, res) => {
	let newUser = req.body.user;
	let foundUser = [];
	try {
		await services.users.updateUser(newUser);
		await services.adresse.updateUserAdresse(newUser);
		foundUser = await services.users.getUser(newUser.id);
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json(foundUser);
};
/**
 * Supprime un utilisateur de la base de données
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retourné par le service ou la réponse
 */
const deleteUser = async (req, res) => {
	try {
		await services.users.deleteUser(req.params.mail);
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json();
}

/**
 * Récupère la liste des utilisateurs par nom croissant
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou la liste des utilisateurs
 */
const getListUsers = async (req, res) => {
	let usersList = [];

	try {
		usersList = await services.users.getListUsers();
	} catch (error) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json(usersList);
}

/**
 * Récupère la liste des utilisateurs suivant un rôle
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou la liste des utilisateurs
 */
const getListUsersWithIdRole = async (req, res) => {
	let usersListWithIdRole = [];

	try {
		usersListWithIdRole = await services.users.getListUsersWithIdRole(req.params.idRole);
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json(usersListWithIdRole);
}

const getListUsersEmailWithIdRole = async(req, res) => {
	let usersEmailListWithIdRole = [];

	try {
		usersEmailListWithIdRole = await services.users.getListUsersEmailWithIdRole(req.params.idRole);
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json(usersEmailListWithIdRole);
}

/**
 * Ajoute un nouvel utilisateur
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou une confirmation (code 200)
 */
const addUser = async (req, res) => {
	let user = req.body.user;
	try {
		await services.users.addUser(user);
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json();
}

const getUser = async (req, res) => {
	let queryRes = null;
	try {
		queryRes = await services.users.getUser(req.params.userId);
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json(queryRes);
};

/**
 * Récupère les propriétaires en base de données afin de les afficher dans une liste déroulante
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou les propriétaires trouvées
 */
const listeProprietaire = async (req, res) => {
	let foundProprietaire = [];

	try {
		foundProprietaire = await services.users.getListeProprietaire();
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(foundProprietaire);
};


module.exports = {
	addUser,
	updateUser,
	deleteUser,
	getListUsers,
	getListUsersWithIdRole,
	getListUsersEmailWithIdRole,
	getUser,
	listeProprietaire,
}
