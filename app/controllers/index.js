const bien = require("./bien.controller");
const images = require("./images.controller");
const file = require("./file.controller");
const users = require("./users.controller");
const address = require("./address.controller");
const register = require("./register.controller");
const bail = require("./bail.controller");
const auth = require("./auth.controller");
const pieces = require("./pieces.controller");

module.exports = {
    users,
    address,
    register,
    bien,
    images,
    file,
    users,
    bail,
    auth,
    pieces
}
