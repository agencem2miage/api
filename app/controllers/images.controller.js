"use strict";

const services = require('../services');

/**
 * Récupère les images d'un bien
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou les images trouvée
 */
const getImages = async (req, res) => {
	let foundImages = [];

	try {
		foundImages = await services.images.getImages(req.params.bienId);
	} catch (err) {
		return services.exception.generateException(err, res);
	}
	res.status(200).json(foundImages);
};


module.exports = {
	getImages,
}