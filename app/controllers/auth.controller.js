"use strict";

const services = require('../services');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const md5 = require('md5');

/**
 * Récupère les informations de l'administrateur et crée un nouveau JWT
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns une erreur, ou le token d'authentification de l'administrateur ainsi que ses informations 
 */
const loginAdmin = async (req, res) => {
	let foundUser = [];

	let ipClient = req.headers["x-forwarded-for"] ? req.headers["x-forwarded-for"].split(',')[0] : req.ip;

	try {
		foundUser = await services.auth.loginAdmin(req.body.login);

		if (foundUser.length === 0) {
			throw new services.exception.httpException('CLIENT_016');
		}
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	const isPasswordValid = bcrypt.compareSync(
		req.body.passwordCrypt,
		foundUser[0].USER_mdp
	);

	if (!isPasswordValid) {
		return services.exception.generateException(new services.exception.httpException('CLIENT_016'), res);
	}

	const token = jwt.sign(
		{
			id: foundUser[0].USER_id,
			mail: foundUser[0].USER_mail,
			ipClient
		},
		process.env.JWT_SECRET,
		{
			expiresIn: "7d"
		}
	);

	let securityUser = {
		USER_id: foundUser[0].USER_id,
		USER_civilite: foundUser[0].USER_civilite,
		USER_prenom: foundUser[0].USER_prenom,
		USER_nom: foundUser[0].USER_nom,
		USER_mail: foundUser[0].USER_mail,
		USER_tel: foundUser[0].USER_tel,
		USER_dateNaissance: foundUser[0].USER_dateNaissance,
		USER_lieuNaissance: foundUser[0].USER_lieuNaissance,
		ROLE_id: foundUser[0].ROLE_id,
		ROLE_label: foundUser[0].ROLE_label,
		ADRESSE_id: foundUser[0].ADRESSE_id,
		ADRESSE_numero: foundUser[0].ADRESSE_numero,
		ADRESSE_rue: foundUser[0].ADRESSE_rue,
		ADRESSE_complement: foundUser[0].ADRESSE_complement,
		ADRESSE_codePostal: foundUser[0].ADRESSE_codePostal,
		ADRESSE_ville: foundUser[0].ADRESSE_ville,
		ADRESSE_pays: foundUser[0].ADRESSE_pays
	}

	res.status(200).json({
		user: securityUser,
		authenticationToken: token
	});
};

/**
 * Récupère les informations de l'administrateur et crée un nouveau JWT
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns une erreur, ou le token d'authentification de l'administrateur ainsi que ses informations 
 */
 const loginUser = async (req, res) => {
	let foundUser = [];

	let ipClient = req.headers["x-forwarded-for"] ? req.headers["x-forwarded-for"].split(',')[0] : req.ip;

	try {
		foundUser = await services.auth.loginUser(req.body.login);

		if (foundUser.length === 0) {
			throw new services.exception.httpException('CLIENT_016');
		}
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	const isPasswordValid = bcrypt.compareSync(
		req.body.passwordCrypt,
		foundUser[0].USER_mdp
	);

	if (!isPasswordValid) {
		return services.exception.generateException(new services.exception.httpException('CLIENT_016'), res);
	}

	const token = jwt.sign(
		{
			id: foundUser[0].USER_id,
			mail: foundUser[0].USER_mail,
			ipClient
		},
		process.env.JWT_SECRET,
		{
			expiresIn: "7d"
		}
	);

	let securityUser = {
		USER_id: foundUser[0].USER_id,
		USER_civilite: foundUser[0].USER_civilite,
		USER_prenom: foundUser[0].USER_prenom,
		USER_nom: foundUser[0].USER_nom,
		USER_mail: foundUser[0].USER_mail,
		USER_tel: foundUser[0].USER_tel,
		USER_dateNaissance: foundUser[0].USER_dateNaissance,
		USER_lieuNaissance: foundUser[0].USER_lieuNaissance,
		ROLE_id: foundUser[0].ROLE_id,
		ROLE_label: foundUser[0].ROLE_label,
		ADRESSE_id: foundUser[0].ADRESSE_id,
		ADRESSE_numero: foundUser[0].ADRESSE_numero,
		ADRESSE_rue: foundUser[0].ADRESSE_rue,
		ADRESSE_complement: foundUser[0].ADRESSE_complement,
		ADRESSE_codePostal: foundUser[0].ADRESSE_codePostal,
		ADRESSE_ville: foundUser[0].ADRESSE_ville,
		ADRESSE_pays: foundUser[0].ADRESSE_pays
	}

	res.status(200).json({
		user: securityUser,
		authenticationToken: token
	});
};

module.exports = {
	loginAdmin,
	loginUser
}