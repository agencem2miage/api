"use strict";

const services = require('../services');

/**
 * Récupère l'ensemble des pièces d'un bien
 * @param {*} req Requête 
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou l'ensemble des pièces
 */
const getPieces = async (req, res) => {
	let pieces = null;

	try {
		pieces = await services.pieces.getPieces(req.params.bienId);
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json(pieces);
}

/**
 * Récupère l'ensemble des pièces suivant leur type
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou l'ensembles des pièces associées à un type
 */
const getTypePieces = async (req, res) => {
	let typePieces = null;

	try {
		typePieces = await services.pieces.getTypePieces();
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json(typePieces);
}

module.exports = {
    getPieces,
	getTypePieces
}