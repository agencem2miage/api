"use strict";

const services = require('../services');

/**
 * Récupère la liste des fichiers de location
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou la liste des fichiers de location
 */
const getListeFileLocation = async (req, res) => {
	let listeIdBail = req.body.bailId;
	let listeFileBail = [];
	let listeFile = {
		listFileBail: '',
		listFileEtatLieu: '',
		listFileAssuranceProprietaire: '',
		listFileAssuranceLocataire: '',
		listFileContratGestion: '',
		listFileAutre: '',
	};

	const path = './document-location/dossier-numero-';


	try {
		for (let i = 0; i<listeIdBail.length; i++){
		listeFile.listFileBail = await services.file.getFileFromFolder(path + listeIdBail[i] + '/1.bail/');
		listeFile.listFileEtatLieu = await services.file.getFileFromFolder(path + listeIdBail[i] + '/2.etat-lieu/');
		listeFile.listFileAssuranceProprietaire = await services.file.getFileFromFolder(path + listeIdBail[i] + '/3.assurance-proprietaire/');
		listeFile.listFileAssuranceLocataire = await services.file.getFileFromFolder(path + listeIdBail[i] + '/4.assurance-proprietaire/');
		listeFile.listFileContratGestion = await services.file.getFileFromFolder(path + listeIdBail[i] + '/5.contrat-gestion/');
		listeFile.listFileDiagEnergie = await services.file.getFileFromFolder(path + listeIdBail[i] + '/6.diagnostique-energetique/');
		listeFile.listFileAutre = await services.file.getFileFromFolder(path + listeIdBail[i] + '/7.autre/');
		listeFileBail.push(listeFile);
		listeFile = {
			listFileBail: '',
			listFileEtatLieu: '',
			listFileAssuranceProprietaire: '',
			listFileAssuranceLocataire: '',
			listFileContratGestion: '',
			listFileAutre: '',
		};
		} 
	}catch (err) {
			return services.exception.generateException(err, res);
		}
	res.status(200).json(listeFileBail);
	};


/**
 * Modifie le contenu d'une actualité
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée par le service ou une confirmation (code 200)
 */
const updateFileLocation = async (req, res) => {
	let listFile = req.body.listFile;
	const path = './document-location/dossier-numero-' + req.body.bailId;
	try {
		if (listFile.listFileBail && listFile.listFileBail !== '') {
			for (let i = 0; i < listFile.listFileBail.length; i++) {
				let result = await services.file.saveFile(listFile.listFileBail[i], path + '/1.bail/');
			}
		}
		if (listFile.listFileEtatLieu && listFile.listFileEtatLieu !== '') {
			for (let i = 0; i < listFile.listFileEtatLieu.length; i++) {
				let result = await services.file.saveFile(listFile.listFileEtatLieu[i], path + '/2.etat-lieu/');
			}
		}
		if (listFile.listFileAssuranceProprietaire && listFile.listFileAssuranceProprietaire !== '') {
			for (let i = 0; i < listFile.listFileAssuranceProprietaire.length; i++) {
				let result = await services.file.saveFile(listFile.listFileAssuranceProprietaire[i], path + '/3.assurance-proprietaire/');
			}
		}
		if (listFile.listFileAssuranceLocataire && listFile.listFileAssuranceLocataire !== '') {
			for (let i = 0; i < listFile.listFileAssuranceLocataire.length; i++) {
				let result = await services.file.saveFile(listFile.listFileAssuranceLocataire[i], path + '/4.assurance-proprietaire/');
			}
		}
		if (listFile.listFileContratGestion && listFile.listFileContratGestion !== '') {
			for (let i = 0; i < listFile.listFileContratGestion.length; i++) {
				let result = await services.file.saveFile(listFile.listFileContratGestion[i], path + '/5.contrat-gestion/');
			}
		}
		if (listFile.listFileDiagEnergie && listFile.listFileDiagEnergie !== '') {
			for (let i = 0; i < listFile.listFileDiagEnergie.length; i++) {
				let result = await services.file.saveFile(listFile.listFileDiagEnergie[i], path + '/6.diagnostique-energetique/');
			}
		}
		if (listFile.listFileAutre && listFile.listFileAutre !== '') {
			for (let i = 0; i < listFile.listFileAutre.length; i++) {
				let result = await services.file.saveFile(listFile.listFileAutre[i], path + '/7.autre/');
			}
		}
	} catch (err) {
		return services.exception.generateException(err, res);
	}

	res.status(200).json();
}

/**
 * Renvoi le fichier demandé
 * @param {*} req Requête
 * @param {*} res Réponse
 * @returns L'erreur retournée ou une confirmation (code 200)
 */
const getFile = async (req, res) => {
	var filePath = "pdf/" + req.params.nomFichier;
	var fileName = "fichier.pdf";
	res.setHeader('Content-type', 'application/pdf');
	res.download(filePath, fileName);
};

module.exports = {
	updateFileLocation,
	getListeFileLocation,
	getFile,
}