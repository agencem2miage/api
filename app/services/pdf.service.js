"use strict";

const exception = require('../services/exception.service');
const fs = require('fs');
const util = require('util');

/**
 * Enregistre le fichier PDF reçu dans le dossier /pdf du serveur
 * @param {*} pdf PDF au format base64
 * @returns Le chemin relatif vers le PDF (à partir de la racine du serveur)
 */
 const savePdf = async (pdf, path) => {
	let newPdf = {};

	try {
		newPdf.data = reqBase64ToPdf(pdf);
	} catch (err) {
		throw err;
	}

	newPdf.name = new Date().getTime() + '.pdf';

	// Création du dossier PDF s'il n'existe pas
	createFolder(path)

	// Enregistrement de l'image sur le disque
	try {
		const writeFile = util.promisify(fs.writeFile);
		await writeFile(path + newPdf.name, newPdf.data);
	} catch (err) {
		throw new Error(err);
	}

	return newPdf.name;
};

/**
 * Fonction qui permet de créer un dossier à partir d'un chemin
 * @param {*} path Chemin 
 */
 function createFolder(path) {
	const folder = path.split('/');
	let allFolder = '';
	for (let i = 0; i < folder.length; i++) {
		if (folder[i] !== '.' && folder[i] !== '') {
			allFolder = allFolder + '/' + folder[i];
			if (!fs.existsSync('./' + allFolder + '/')) {
				fs.mkdirSync('./' + allFolder + '/');
			}
		}
	}
}

/**
 * Convertit un PDF au format base64 en fichier
 * @param {*} pdfBase64 Le PDF au format base64
 * @returns Le fichier PDF créé
 */
function reqBase64ToPdf(pdfBase64) {
	let pdf = null;

	if (!pdfBase64.match(/JVBERi0xLjMKJf\/\/\/\//)) {
		throw new exception.httpException('CLIENT_011');
	}

	pdf = new Buffer.from(pdfBase64, 'base64');

	if (parseInt(Buffer.byteLength(pdf)/1024)>2000) {
		throw new exception.httpException('CLIENT_013');
	}

	return pdf;
}

module.exports = {
	savePdf
}