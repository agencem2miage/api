"use strict";

const { transaction } = require('../../database');

/**
 * Récupère les informations d'une adresse suivant un identifiant passé en paramètre
 * @param {*} addressId l'identifiant de l'utilisateur
 * @returns le résultat de la requête
 */
const getAddress = async (addressId) => {
    const query = 'SELECT * ' +
        'FROM adresse ' +
        'WHERE id = ?';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query, parseInt(addressId));
        } catch (err) {
            throw new Error(err)
        }

    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
}

/**
 * Ajoute une nouvelle adresse en base de données
 * @param {*} address l'adresse à rajouter en base
 */
const addAddress = async (address) => {
    const query = 'INSERT INTO ' +
        'adresse ' +
        'SET ?';

    const params = [{
        numero: address.numero,
        rue: address.rue,
        complement: address.complement,
        codePostal: address.codePostal,
        ville: address.ville,
        pays: address.pays
    }];

    await transaction(async connection => {
        try {
            await connection.query(query, params);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });
}

/**
 * Retourne le dernier id de la table adresse (dernière adresse créée)
 * @returns le résultat de la requête, ici le dernier id
 */
const getLastIdCreated = async () => {
    const query = 'SELECT MAX(id) AS LAST_ID FROM adresse';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query);
        } catch (err) {
            throw new Error(err)
        }

    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
}

module.exports = {
    getAddress,
    addAddress,
    getLastIdCreated
}