"use strict";

const { transaction } = require('../../database');

/**
 * Récupère les informations de l'utilisateur
 * @param userId Identifiant de l'utilisateur
 * @returns L'utilisateur
 */
const getUser = async (userId) => {

	const query = 'SELECT * ' +
		'FROM user_view ' +
		'WHERE USER_id = ?';

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, parseInt(userId));
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});
	return queryRes;
}

/**
 * Récupère les propriétaires depuis la BDD
 * @returns Les propriétaires trouvées, une erreur sinon
 */
const getListeProprietaire = async (id) => {
	const query =
		'SELECT u.id as USER_id, civilite as USER_civilite, prenom as USER_prenom, nom as USER_nom, ' +
		'mail as USER_mail, tel as USER_tel, idAdresse as ADRESSE_id, numero as ADRESSE_numero, rue as ADRESSE_rue, ' +
		'complement as ADRESSE_complement, codePostal as ADRESSE_codepostal, ville as ADRESSE_ville, pays as ADRESSE_pays ' +
		'FROM user u join adresse a ' +
		'WHERE idRole = 3 and u.idAdresse = a.id';

	const params = [];

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, params);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});

	return queryRes;
};

/**
 * Modifie le contenu d'une actualité
 * @param {*} newsId Identifiant de l'actualité
 * @returns Une 200 ou une erreur
 */
const updateUser = async (newUser) => {
	const query = 'UPDATE user ' +
		'SET ? ' +
		'WHERE id = ?'

	let [queryRes, fields] = [];
	const params = [
		{
			id: newUser.id,
			civilite: newUser.civilite,
			nom: newUser.nom,
			prenom: newUser.prenom,
			mail: newUser.mail,
			tel: newUser.numeroTelephone,
			dateNaissance: newUser.dateNaissance,
			lieuNaissance: newUser.villeNaissance,
		},
		newUser.id
	];
	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, params);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});

	return queryRes;
}

/**
 * Ajoute un utilisateur dans la base de données
 * @param {*} user La civilité, le prénom, le nom, le mail, le téléphone, la date de naissance, 
 * le lieu de naissance, le mot de passe et le role de l'utilisateur ajouté
 */
const addUser = async (user) => {
	const query = 'INSERT INTO ' +
		'user ' +
		'SET ?';

	const params = [{
		civilite: user.civilite,
		prenom: user.prenom,
		nom: user.nom,
		mail: user.mail,
		tel: user.tel,
		dateNaissance: user.dateNaissance,
		lieuNaissance: user.lieuNaissance,
		mdp: user.mdp,
		idAdresse: user.idAdresse,
		idRole: user.idRole
	}];

	await transaction(async connection => {
		try {
			await connection.query(query, params);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});
}


/**
 * Récupère la liste des utilisateurs suivant leur rôle
 * @param {*} idRole L'identifiant d'un rôle
 * @returns Une liste des utilisateurs / Une erreur sinon
 */
const getListUsersWithIdRole = async (idRole) => {
	const query =
		'SELECT * ' +
		'FROM user_view ' +
		'WHERE ROLE_id = ?';

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, idRole);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});

	return queryRes;
}

/**
 * Supprime un utilisateur de la base de données suivant un identifiant
 * @param {*} userId l'identifiant de l'utilisateur que l'on veut supprimer
 * @returns le résutlat de la requête
 */
const deleteUser = async (mail) => {

	// TODO : Faire la recherche d'id

	const queryGetId = 'SELECT id ' +
	'FROM user ' +
	'WHERE mail = ?';

	const queryDelete = 'DELETE FROM user ' +
		'WHERE id = ?';


	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(queryGetId, mail);
			[queryRes, fields] = await connection.query(queryDelete, queryRes[0].id);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});

	return queryRes;
}


/**
 * Récupère la liste des utilisateurs
 * @returns La liste des utilisateurs dans l'ordre des noms ascendant
 */
const getListUsers = async () => {
	const query =
		'SELECT * ' +
		'FROM user_view ' +
		'ORDER BY USER_nom ASC';

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});

	return queryRes;
}

/**
 * Récupère la liste des utilisateurs suivant leur rôle
 * @param {*} idRole L'identifiant d'un rôle
 * @returns Une liste des utilisateurs / Une erreur sinon
 */
 const getListUsersEmailWithIdRole = async (idRole) => {
	const query =
		'SELECT USER_mail ' +
		'FROM user_view ' +
		'WHERE ROLE_id = ?';

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, idRole);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});

	return queryRes;
}


module.exports = {
	getUser,
	deleteUser,
	getListUsers,
	getListUsersWithIdRole,
	addUser,
	updateUser,
	getListUsersEmailWithIdRole,
	getListeProprietaire,
}
