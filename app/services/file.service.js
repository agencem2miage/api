"use strict";

const exception = require('./exception.service');
const fs = require('fs');
const util = require('util');

/**
 * Enregistre l'image reçue dans le dossiers /images du serveur
 * @param {*} image Image au format base64
 * @returns Le chemin relatif vers l'image (à partir de la racine du serveur)
 */
const saveFile = async (file, path) => {
	let newFile = null;

	try {
		newFile = reqBase64ToFile(file);
	} catch (err) {
		throw err;
	}

	newFile.name = new Date().getTime() + '.' + newFile.type[1];

	// Création du dossier d'images s'il n'existe pas
	createFolder(path);


	// Enregistrement de l'image sur le disque
	try {
		const writeFile = util.promisify(fs.writeFile);
		await writeFile(path + newFile.name, newFile.data);
	} catch (err) {
		throw new Error(err);
	}
	return newFile.name;
};

/**
 * Convertit une image au format base64 en fichier
 * @param {*} fileBase64 L'image au format base64
 * @returns Le fichier image créé
 */
function reqBase64ToFile(fileBase64) {

	const matches = fileBase64.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
	const file = {};

	if (!matches || matches.length !== 3) {
		throw new exception.httpException('CLIENT_011');
	}

	file.type = matches[1].split('/');
	file.data = new Buffer.from(matches[2], 'base64');

	if (file.type[1].toLowerCase() !== 'pdf') {
		throw new exception.httpException('CLIENT_011');
	}

	if (parseInt(Buffer.byteLength(file.data) / 1024) > 5000) {
		throw new exception.httpException('CLIENT_013');
	}

	return file;
}

/**
 * Fonction qui permet de créer un dossier à partir d'un chemin
 * @param {*} path Chemin 
 */
function createFolder(path) {
	const folder = path.split('/');
	let allFolder = '';
	for (let i = 0; i < folder.length; i++) {
		if (folder[i] !== '.' && folder[i] !== '') {
			allFolder = allFolder + '/' + folder[i];
			if (!fs.existsSync('./' + allFolder + '/')) {
				fs.mkdirSync('./' + allFolder + '/');
			}
		}
	}
}

/**
 * Récupère la liste des fichiers à partir d'un dossier
 * @param {*} path Chemin vers le fichier
 * @returns La liste de fichiers d'un dossier
 */
const getFileFromFolder = async (path) => {
	const list = [];
	const pathFolder = path;
	let isFolder = true;
	try {

		const folder = pathFolder.split('/');
		let allFolder = '';
		for (let i = 0; i < folder.length; i++) {
			if (folder[i] !== '.' && folder[i] !== '') {
				allFolder = allFolder + '/' + folder[i];
				if (!fs.existsSync('./' + allFolder + '/')) {
					isFolder = false;
				}
			}
		}
		if (isFolder) {
			list.push(fs.readdirSync(path).map(fileName => {
				return fileName;
			}))
		}


	} catch (err) {
		throw new Error(err);
	}
	return list;
}


/**
 * Enregistre l'image reçue dans le dossiers /images du serveur
 * @param {*} image Image au format base64
 * @returns Le chemin relatif vers l'image (à partir de la racine du serveur)
 */
const saveImage = async (image) => {
	let newImage = null;

	try {
		newImage = reqBase64ToImage(image);
	} catch (err) {
		throw err;
	}

	newImage.name = new Date().getTime() + '.' + newImage.type[1];

	// Création du dossier d'images s'il n'existe pas
	if (!fs.existsSync('./images/')) {
		fs.mkdirSync('./images/');
	}

	// Enregistrement de l'image sur le disque
	try {
		const writeFile = util.promisify(fs.writeFile);
		await writeFile('./images/' + newImage.name, newImage.data);
	} catch (err) {
		throw new Error(err);
	}

	return newImage.name;
};

/**
 * Convertit une image au format base64 en fichier
 * @param {*} imageBase64 L'image au format base64
 * @returns Le fichier image créé
 */
function reqBase64ToImage(imageBase64) {
	const matches = imageBase64.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
	const image = {};

	if (!matches || matches.length !== 3) {
		throw new exception.httpException('CLIENT_011');
	}

	image.type = matches[1].split('/');
	image.data = new Buffer.from(matches[2], 'base64');

	if (image.type[1].toLowerCase() !== 'jpeg' && image.type[1].toLowerCase() !== 'jpg' && image.type[1].toLowerCase() !== 'png') {
		throw new exception.httpException('CLIENT_011');
	}

	if (parseInt(Buffer.byteLength(image.data) / 1024) > 2000) {
		throw new exception.httpException('CLIENT_013');
	}

	return image;
}

module.exports = {
	saveFile,
	saveImage,
	getFileFromFolder
}