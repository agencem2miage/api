"use strict";

const { transaction } = require('../../database');

/**
 * Ajoute un nouvel utilisateur en base
 * @param {*} address L'adresse qu'il faut insérer
 * @param {*} user L'utilisateur qu'il faut insérer 
 */
const register = async (address, user) => {

    const bcrypt = require('bcrypt');
    const saltRounds = 10;
    const hash = bcrypt.hashSync(Math.random().toString(36).slice(-8), saltRounds);
    let [queryRes, fields] = [];

    const queryAddress = 'INSERT INTO ' +
        'adresse ' +
        'SET ?';

    const paramsAddress = [{
        numero: address.numero,
        rue: address.rue,
        complement: address.complement,
        codePostal: address.codePostal,
        ville: address.ville,
        pays: address.pays
    }];

    const queryUser = 'INSERT INTO ' + 
	'user ' +
	'SET ?';

	const paramsUser = [{
		civilite: user.civilite,
		prenom: user.prenom,
		nom: user.nom,
		mail: user.mail,
		tel: user.numeroTelephone,
		dateNaissance: user.dateNaissance,
		lieuNaissance: user.villeNaissance,
		mdp: hash,
		idAdresse: -1,
		idRole: user.role.id
	}];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(queryAddress, paramsAddress);
            paramsUser[0].idAdresse = queryRes.insertId;
            await connection.query(queryUser, paramsUser);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });
}



module.exports = {
    register,
}