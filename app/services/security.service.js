"use strict";
const rateLimit = require("express-rate-limit");
const exception = require('./exception.service');

/**
 * Fonction qui permet de générer une erreur si un utilisateur génère trop de requête
 * @param {*} req Requête
 * @param {*} res Réponse
 */
const limitReached = (req, res) => {
	exception.generateException(new exception.httpException('CLIENT_004'), res);
}

const apiLimiter = rateLimit({
	windowMs: 1000, // 1 minutes
	max: 50,
	handler: limitReached
});

module.exports = {
	apiLimiter
}