"use strict";

const { transaction } = require("../../database");

/**
 * Crée un nouveau bail
 * @param {*} bail Bail passé en JSON
 * @returns Une erreur ou une 200
 */
const postBail = async (bail) => {
	const queryBail =
		'INSERT INTO bail SET ?';

	const paramsBail = [{
		datePriseEffet: bail.datePriseEffet,
		dureeBail: bail.dureeBail,
		dureeBailReduite: bail.dureeBailReduite,
		justificationDureeBailReduite: bail.justificationDureeBailReduite,
		periodicite: bail.periodicite,
		loyerHorsCharges: bail.loyerHorsCharges,
		chargesRecuperables: bail.chargesRecuperables,
		isLoyerReference: bail.isLoyerReference,
		isMontantMaximumAnnuel: bail.isMontantMaximumAnnuel,
		loyerReference: bail.loyerReference,
		loyerAncienLocataire: bail.loyerAncienLocataire,
		modaliteReglementChargesRecuperables: bail.modaliteReglementChargesRecuperables,
		montantChargesRecuperables: bail.montantChargesRecuperables,
		souscriptionAssuranceParBailleur: bail.souscriptionAssuranceParBailleur,
		periodicitePaiementLoyer: bail.periodicitePaiementLoyer,
		echeancePaiement: bail.echeancePaiement,
		datePeriodePaiement: bail.datePeriodePaiement,
		montantPremiereEcheance: bail.montantPremiereEcheance,
		montantRecuperableAssurance: bail.montantRecuperableAssurance,
		montantRecuperableAssuranceDouzieme: bail.montantRecuperableAssuranceDouzieme,
		montantGaranties: bail.montantGaranties,
		usageLocaux: bail.usageLocaux,
		signatureProprietaire: bail.signatureProprietaire,
		signatureLocataire: bail.signatureLocataire,
		pdf: '',
		idBien: bail.bien,
		idLocataire: bail.locataire,
	}];

	const queryMajorations =
		'INSERT INTO majoration (idBien, natureTravaux, delai, modalite, montant) VALUES ?';

	const paramsMajorations = [];
	Object.values(bail.majorations).forEach(majoration => paramsMajorations.push([bail.bien, majoration.nature, majoration.delaiRealisation, majoration.modalites, majoration.montant]));

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(queryBail, paramsBail);

			if (paramsMajorations.length > 0) {
				[queryRes, fields] = await connection.query(queryMajorations, [paramsMajorations]);
			}
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});
	return queryRes;
};


/**
 * Renvoie la liste des biens
 * @returns La liste des biens trouvées, une erreur sinon
 */
const getBail = async () => {
	const query =
		'SELECT id, idBien ' +
		'FROM bail ';

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});
	return queryRes;
};

/**
 * Récupère le bail d'un agent
 * @param {*} id l'identifiant du bail
 * @returns le bail d'un agent, une erreur sinon
 */
const getBailAgent = async (id) => {
	const query =
		'SELECT * FROM bail_view WHERE BAIL_id = ?'

	const params = [parseInt(id)];

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, params);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});
	return queryRes;
};

/**
 * Récupère le bail d'un propriétaire 
 * @param {*} id L'identifant du propriétaire
 * @returns L'identifiant d'un bail d'un propriétaire, une erreur sinon
 */
const getBailProprietaireId = async (id) => {
	const query =
		'SELECT bail.id ' +
		'FROM bail left join bien on bail.idBien = bien.id ' +
		'where bien.idProprietaire = ?' ;

	const params = [parseInt(id)];

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, params);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});
	return queryRes;
};
 /**
  * Récupère la liste des bails
  * @returns La liste des bails, une erreur sinon
  */
const getBails = async () => {
	const query = 'SELECT * FROM bail_view';

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});
	return queryRes;
};

 /**
  * Récupère 
  * @param {*} id identifiant du locataire
  * @returns L'identifiant d'un bail de locataire, une erreur sinon
  */
const getBailLocataireId = async (id) => {
	const query =
		'SELECT bail.id ' +
		'FROM bail ' +
		'where bail.idLocataire = ?' ;

	const params = [parseInt(id)];

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, params);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});
	return queryRes;
};

const updateBailPdf = async (id, pdfName) => {
	const query = 'UPDATE bail SET ? WHERE id = ?';
	const params = [
		{
			pdf: pdfName
		},
		id
	];

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, params);
		} catch (err) {
			throw new Error(err);
		}
	})
	.catch((err) => {
		throw err;
	});

	return queryRes;
};

module.exports = {
	postBail,
	getBails,
	getBail,
	getBailAgent,
	getBailProprietaireId,
	getBailLocataireId,
	updateBailPdf
}