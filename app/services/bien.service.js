"use strict";

const { transaction } = require("../../database");

/**
 * Récupère un bien depuis la BDD
 * @param {*} id Identifiant du bien
 * @returns Le bien trouvé, une erreur sinon
 */
const getBien = async (id, userId) => {
    const query =
        'SELECT BIEN_ID, TYPE_CONTRAT_LABEL, TYPE_BIEN_LABEL, NB_PIECE, ADRESSE_BIEN_CODE_POSTAL, ADRESSE_BIEN_VILLE, ' +
        'BIEN_PRIX_LOCATION, SURFACE_HABITABLE, NB_PIECE, NB_CHAMBRE, NB_SALLE_DE_BAIN, DESCRIPTION, DETAILS_PIECE, ' +
        'EQUIPEMENT, REGIME_JURIDIQUE_LABEL, ANNEE_CONSTRUCTION, MEUBLE, CLASSE_ENERGIE, GES, SURFACE_TERRAIN, ' +
        'SURFACE_BALCON, GARAGE_VOITURE, GARAGE_VELO, ESPACES_VERTS, LOCAL_POUBELLE, ASCENSEUR, AIRE_JEUX, LAVERIE, ' +
        'GARDIENNAGE ' +
        'FROM bien_view ' +
        'WHERE bien_view.BIEN_ID = ?';

    const params = [parseInt(id)];

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query, params);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });
    return queryRes;
};

/**
 * Renvoie la liste des biens
 * @returns La liste des biens trouvées, une erreur sinon
 */
const getListeBien = async () => {
    const query =
        'SELECT BIEN_ID, BIEN_PRIX_LOCATION, DESCRIPTION, ADRESSE_BIEN_VILLE, ' +
        'ID_TYPE_CONTRAT, TYPE_CONTRAT_LABEL, ID_TYPE_BIEN, NB_PIECE, TYPE_BIEN_LABEL, BIEN_ID_ADRESSE, SURFACE_HABITABLE, PREMIERE_IMAGE, USER_NOM ' +
        'FROM bien_view';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
};

/**
 * Renvoie la liste des biens avec le détail
 * @returns La liste des biens trouvées, une erreur sinon
 */
const getListeBienDetail = async () => {
    const query =
        'SELECT * FROM bien_view';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
};

/**
 * Renvoie un bien avec le détail
 * @returns Le bien trouvé, une erreur sinon
 */
const getBienDetail = async (id) => {
    const query =
        'SELECT * FROM bien_view WHERE BIEN_ID = ?';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query, [id]);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
};

/**
 * Renvoie la liste des biens
 * @returns La liste des biens trouvées, une erreur sinon
 */
const getListeTypeContrat = async () => {
    const query =
        'SELECT * ' +
        'FROM type_contrat';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });
    return queryRes;
};

/**
 * Renvoie la liste des biens
 * @returns La liste des biens trouvées, une erreur sinon
 */
const getListeTypeBien = async () => {
    const query =
        'SELECT * ' +
        'FROM type_bien';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
};

/**
 * Renvoie la liste des biens
 * @returns La liste des biens trouvées, une erreur sinon
 */
const getListeLocalisation = async () => {
    const query =
        'SELECT id, ville ' +
        'FROM adresse ' +
        'GROUP BY adresse.ville ';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
};

/**
 * Récupère le bien d'un agent
 * @param {*} id l'identifiant du bien 
 * @returns le bien cherché, une erreur sinon
 */
const getBienAgent = async (id) => {
    const query =
        'SELECT BIEN_ID, TYPE_CONTRAT_LABEL, ID_TYPE_CONTRAT, TYPE_BIEN_LABEL, ID_TYPE_BIEN, NB_PIECE, ADRESSE_BIEN_CODE_POSTAL, ADRESSE_BIEN_VILLE, ' +
        'BIEN_PRIX_LOCATION, SURFACE_HABITABLE, NB_PIECE, NB_CHAMBRE, NB_SALLE_DE_BAIN, DESCRIPTION, DETAILS_PIECE, ' +
        'EQUIPEMENT, REGIME_JURIDIQUE_LABEL, ID_REGIME_JURIDIQUE, ANNEE_CONSTRUCTION, MEUBLE, CLASSE_ENERGIE, GES, SURFACE_TERRAIN, ' +
        'SURFACE_BALCON, GARAGE_VOITURE, GARAGE_VELO, ESPACES_VERTS, LOCAL_POUBELLE, ASCENSEUR, AIRE_JEUX, LAVERIE, ' +
        'GARDIENNAGE, ' +
        'USER_CIVILITE, USER_PRENOM, USER_NOM, ID_PROPRIETAIRE, USER_MAIL, USER_TEL,  ' +
        'BIEN_ID_ADRESSE, ADRESSE_BIEN_NUMERO, ADRESSE_BIEN_RUE, ADRESSE_BIEN_COMPLEMENT, ADRESSE_BIEN_CODE_POSTAL, ADRESSE_BIEN_VILLE, ADRESSE_BIEN_PAYS ' +
        'BAIL_ID, LOCATAIRE_ID, LOCATAIRE_CIVILITE, LOCATAIRE_PRENOM, LOCATAIRE_NOM, LOCATAIRE_MAIL, LOCATAIRE_TEL, LOCATAIRE_ID_ADRESSE ' +
        'FROM bien_view ' +
        'WHERE bien_view.BIEN_ID = ?';

    const params = [parseInt(id)];

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query, params);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });
    return queryRes;
};

/**
 * Renvoie la liste des biens
 * @returns La liste des biens trouvées, une erreur sinon
 */
const getListeProprietaire = async () => {
    const query =
        'SELECT USER_nom ' +
        'FROM user_view ' +
        'WHERE ROLE_id = 3';

    let [queryRes, fields] = [];

    await transaction(async connection => {
        try {
            [queryRes, fields] = await connection.query(query);
        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });

    return queryRes;
};

/**
 * Modifie le contenu d'un bien
 * @param {*} bienId Identifiant du bien
 * @returns Une 200 ou une erreur
 */
const putEditBien = async (newBien) => {
    const query = 'UPDATE bien ' +
        'SET ? ' +
        'WHERE id = ?'

    const params = [
        {
            prixLocation: newBien.prix,
            anneeConstruction: newBien.anneeConstruction,
            surfacehabitable: newBien.surfaceHabitable,
            surfaceTerrain: newBien.surfaceTerrain,
            surfaceBalcon: newBien.surfaceBalcon,
            equipement: newBien.equipementMaison,
            detailsPiece: newBien.detailPieces,
            description: newBien.description,
            ges: newBien.ges,
            classeEnergie: newBien.classeEnergie,
            meuble: newBien.meuble,
            garageVelo: newBien.garageVelo,
            garageVoiture: newBien.garageVoiture,
            espacesVerts: newBien.espacesVerts,
            localPoubelle: newBien.localPoubelles,
            ascenseur: newBien.ascenseur,
            aireJeux: newBien.aireJeux,
            gardiennage: newBien.gardiennage,
            laverie: newBien.laverie,
            idRegimeJuridique: newBien.regimeJuridique.id,
            idTypeContrat: newBien.typeContrat.id,
            idTypeBien: newBien.typeBien.id,
            idProprietaire: newBien.proprietaire.id,
        },
        newBien.id
    ];

    const query2 = 'UPDATE adresse ' +
        'SET ? ' +
        'WHERE id = ?'

    const param2 = [
        {
            numero: newBien.adresse.numero,
            rue: newBien.adresse.rue,
            complement: newBien.adresse.complement,
            codePostal: newBien.adresse.codePostal,
            ville: newBien.adresse.ville,
            pays: newBien.adresse.pays
        },
        newBien.adresse.id
    ];

    await transaction(async connection => {
        try {
            await connection.query(query, params);
            await connection.query(query2, param2);

        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });

    return;
}


/**
 * Ajoute un utilisateur dans la base de données
 * @param {*} user La civilité, le prénom, le nom, le mail, le téléphone, la date de naissance, 
 * le lieu de naissance, le mot de passe et le role de l'utilisateur ajouté
 */
const postCreationBien = async (newBien) => {
    let [queryResAddress, fieldsAddress] = [];
    let [queryResBien, fieldsBien] = [];

    const queryAddress = 'INSERT INTO ' +
        'adresse ' +
        'SET ?';

    const paramsAddress = [{
        numero: newBien.adresse.numero,
        rue: newBien.adresse.rue,
        complement: newBien.adresse.complement,
        codePostal: newBien.adresse.codePostal,
        ville: newBien.adresse.ville,
        pays: newBien.adresse.pays
    }];


    const queryBien = 'INSERT INTO ' +
        'bien ' +
        'SET ?';
    const paramsBien = [{
        prixLocation: newBien.prix,
        anneeConstruction: newBien.anneeConstruction,
        surfacehabitable: newBien.surfaceHabitable,
        surfaceTerrain: newBien.surfaceTerrain,
        surfaceBalcon: newBien.surfaceBalcon,
        equipement: newBien.equipementMaison,
        detailsPiece: newBien.detailPieces,
        description: newBien.description,
        ges: newBien.ges,
        classeEnergie: newBien.classeEnergie,
        meuble: newBien.meuble,
        garageVelo: newBien.garageVelo,
        garageVoiture: newBien.garageVoiture,
        espacesVerts: newBien.espacesVerts,
        localPoubelle: newBien.localPoubelles,
        ascenseur: newBien.ascenseur,
        aireJeux: newBien.aireJeux,
        gardiennage: newBien.gardiennage,
        laverie: newBien.laverie,
        idRegimeJuridique: newBien.regimeJuridique.id,
        idTypeContrat: newBien.typeContrat.id,
        idTypeBien: newBien.typeBien.id,
        idProprietaire: newBien.proprietaire.id,
        idAdresse: -1,
        assuranceLocataire: ''

    }];

    let queryImage = []
    let paramsImage = []
    if (!newBien.image.includes('undefined')) {
        for (let i = 0; i < newBien.image.length; i++) {
            queryImage[i] = 'INSERT INTO ' +
                'image ' +
                'SET ?';

            paramsImage[i] = [{
                label: newBien.image[i],
                idBien: -1,
            }];

        }
    }

    let queryPiece = []
    let paramsPiece = []

    for (let i = 0; i < newBien.pieces.length; i++) {
        queryPiece[i] = 'INSERT INTO ' +
            'piece_bien ' +
            'SET ?';

        paramsPiece[i] = [{
            idTypePiece: newBien.pieces[i].id,
            idBien: -1,
        }];

    }


    await transaction(async connection => {
        try {
            [queryResAddress, fieldsAddress] = await connection.query(queryAddress, paramsAddress);
            paramsBien[0].idAdresse = queryResAddress.insertId;
            [queryResBien, fieldsBien] = await connection.query(queryBien, paramsBien);
            if (!newBien.image.includes('undefined')) {
                for (let i = 0; i < newBien.image.length; i++) {
                    paramsImage[i][0].idBien = queryResBien.insertId;
                    await connection.query(queryImage[i], paramsImage[i]);
                }
            }
            for (let i = 0; i < newBien.pieces.length; i++) {
                paramsPiece[i][0].idBien = queryResBien.insertId;
                await connection.query(queryPiece[i], paramsPiece[i]);
            }

        } catch (err) {
            throw new Error(err);
        }
    })
        .catch((err) => {
            throw err;
        });
}



module.exports = {
    getBien,
    getBienDetail,
    getListeBien,
    getListeBienDetail,
    getListeTypeBien,
    getListeTypeContrat,
    getListeLocalisation,
    getListeProprietaire,
    getBienAgent,
    putEditBien,
    postCreationBien,
}