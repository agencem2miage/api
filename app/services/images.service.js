"use strict";

const { transaction } = require("../../database");

/**
 * Récupère les images depuis la BDD
 * @param {*} id Identifiant du bien
 * @returns Les images trouvées, une erreur sinon
 */
const getImages = async (id) => {
	const query =
		'SELECT label ' +
		'FROM image ' +
		'WHERE idBien = ?';

	const params = [parseInt(id)];

	let [queryRes, fields] = [];

	await transaction(async connection => {
		try {
			[queryRes, fields] = await connection.query(query, params);
		} catch (err) {
			throw new Error(err);
		}
	})
		.catch((err) => {
			throw err;
		});

	return queryRes;
};


module.exports = {
	getImages,
}