"use strict";

const Validator = require('validatorjs');
const services = require('../services');
const constants = require('../../constants');

/**
 * Contrôle les données reçues avant leur passage au contrôleur.
 * @param {*} req Requête contenant le body
 * @param {*} res Réponse
 * @param {*} next Passe la main au middleware suivant, ou au contrôleur
 * @returns Une erreur CLIENT_010 si certaines données sont corrompues
 */
const signedBailControl = (req, res, next) => {
	const bail = req.body.bail;

	if (!bail) {
		return services.exception.generateException(new services.exception.httpException('CLIENT_010'), res);
	}

	const data = {
		datePriseEffet: bail.datePriseEffet,
		dureeBail: bail.dureeBail,
		dureeBailReduite: bail.dureeBailReduite,
		justificationDureeBailReduite: bail.justificationDureeBailReduite,
		periodicite: bail.periodicite,
		loyerHorsCharges: bail.loyerHorsCharges,
		chargesRecuperables: bail.chargesRecuperables,
		isLoyerReference: bail.isLoyerReference,
		isMontantMaximumAnnuel: bail.isMontantMaximumAnnuel,
		loyerReference: bail.loyerReference,
		loyerAncienLocataire: bail.loyerAncienLocataire,
		modaliteReglementChargesRecuperables: bail.modaliteReglementChargesRecuperables,
		montantChargesRecuperables: bail.montantChargesRecuperables,
		souscriptionAssuranceParBailleur: bail.souscriptionAssuranceParBailleur,
		periodicitePaiementLoyer: bail.periodicitePaiementLoyer,
		echeancePaiement: bail.echeancePaiement,
		datePeriodePaiement: bail.datePeriodePaiement,
		montantPremiereEcheance: bail.montantPremiereEcheance,
		montantRecuperableAssurance: bail.montantRecuperableAssurance,
		montantRecuperableAssuranceDouzieme: bail.montantRecuperableAssuranceDouzieme,
		montantGaranties: bail.montantGaranties,
		usageLocaux: bail.usageLocaux,
		signatureProprietaire: bail.signatureProprietaire,
		signatureLocataire: bail.signatureLocataire,
		pdf: bail.pdf,
		idBien: bail.bien,
		idLocataire: bail.locataire,
		majorations: bail.majorations,
	};

	const rules = {
		datePriseEffet: 'required|date',
		dureeBail: 'required|numeric',
		dureeBailReduite: 'present|string',
		justificationDureeBailReduite: 'present|string',
		periodicite: 'required|string',
		loyerHorsCharges: 'required|numeric',
		chargesRecuperables: 'required|numeric',
		isLoyerReference: 'required|boolean',
		isMontantMaximumAnnuel: 'required|boolean',
		loyerReference: 'required|numeric',
		loyerAncienLocataire: 'required|numeric',
		modaliteReglementChargesRecuperables: 'required|string',
		montantChargesRecuperables: 'present|numeric',
		souscriptionAssuranceParBailleur: 'required|boolean',
		periodicitePaiementLoyer: 'required|string',
		echeancePaiement: 'required|string',
		datePeriodePaiement: 'required|string',
		montantPremiereEcheance: 'required|numeric',
		montantRecuperableAssurance: 'required|numeric',
		montantRecuperableAssuranceDouzieme: 'required|numeric',
		montantGaranties: 'required|numeric',
		usageLocaux: 'required|string',
		signatureProprietaire: 'required|boolean',
		signatureLocataire: 'required|boolean',
		pdf: 'required|string',
		idBien: 'required|integer|min:1',
		idLocataire: 'required|integer|min:1',
		majorations: 'present'
	};

	const validation = new Validator(data, rules);

	if (validation.fails()) {
		return services.exception.generateException(new services.exception.httpException('CLIENT_010'), res);
	}

	next();
};

module.exports = {
	signedBailControl,
}