"use strict";

const Validator = require('validatorjs');

/**
 * Contrôle les données reçues avant leur passage au contrôleur.
 * @param {*} req Requête contenant le body
 * @param {*} res Réponse
 * @param {*} next Passe la main au middleware suivant, ou au contrôleur
 * @returns une erreur si les données ne sont pas bonnes
 */
const usersControl = (req, res, next) => {
    const user = req.body.user;

    try {
        const data = {
            civilité: user.civilité,
            prenom: user.prenom,
            nom: user.nom,
            mail: user.mail,
            tel: user.tel,
            dateNaissance: user.dateNaissance,
            lieuNaissance: user.lieuNaissance,
            mdp: user.mdp,
            idRole: user.idRole
        }

        const rules = {
            civilité: 'required|string',
            prenom: 'required|string',
            nom: 'required|string',
            mail: 'required|string',
            tel: 'required|string',
            dateNaissance: 'required|string',
            mdp: 'required|string',
            idRole: 'required|integer|min:1'
        }

        let validation = new Validator(data, rules);
        if (validation.fails) {
            new Error("Validation d'un utilisateur non valide")
        }
    } catch (error) {

    }

    next();
}

module.exports = {
    usersControl,
}