"use strict";

const express = require('express');
const middlewares = require('../middlewares');
const controllers = require('../controllers');

const router = express.Router();

router.get('/lastId/', controllers.address.getLastIdCreated);

router.get('/:addressId', controllers.address.getAddress);

router.post('/', controllers.address.addAddress);


module.exports = router;