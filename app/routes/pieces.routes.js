"use strict";

const express = require('express');
const middlewares = require('../middlewares');
const controllers = require('../controllers');

const router = express.Router();

router.get('/type-piece', controllers.pieces.getTypePieces);

router.get('/:bienId', controllers.pieces.getPieces);

module.exports = router;