"use strict";

const express = require('express');
const middlewares = require('../middlewares');
const controllers = require('../controllers');

const router = express.Router();

router.get('/filter-bien', controllers.bien.getFilterBien);
router.get('/liste-bien', controllers.bien.getListeBien);
router.get('/liste-bien-detail', controllers.bien.getListeBienDetail);

router.post('/creation-bien', middlewares.auth.isAuthentified, controllers.bien.postCreationBien);
router.get('/liste-bien-admin', middlewares.auth.isAuthentified, controllers.bien.getListeBien);
router.get('/filter-bien-admin', middlewares.auth.isAuthentified, controllers.bien.getFilterAdminBien);


router.get('/:bienId', controllers.bien.getBien);
router.get('/detail/:bienId', controllers.bien.getBienDetail);

router.get('/admin/:bienId', middlewares.auth.isAuthentified, controllers.bien.getBienAgent);
router.put('/admin/:bienId', middlewares.auth.isAuthentified, controllers.bien.putEditBien);


module.exports = router;