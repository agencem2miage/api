"use strict";

const express = require('express');
const middlewares = require('../middlewares');
const controllers = require('../controllers');

const router = express.Router();

router.put('/file-location', middlewares.auth.isAuthentified, controllers.file.updateFileLocation);

router.post('/liste-file-location/', middlewares.auth.isAuthentified, controllers.file.getListeFileLocation);

router.get('/:nomFichier', controllers.file.getFile);

module.exports = router;