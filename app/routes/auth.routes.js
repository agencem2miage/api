"use strict";

const express = require('express');
const controllers = require('../controllers');
const middlewares = require('../middlewares');

const router = express.Router();

router.post('/admin', middlewares.auth.loginControl, controllers.auth.loginAdmin);
router.post('/user', middlewares.auth.loginControl, controllers.auth.loginUser);

// router.post('/connexion-user', middlewares.auth.loginAdminControl, controllers.auth.loginAdmin);

module.exports = router;