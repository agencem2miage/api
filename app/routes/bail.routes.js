"use strict";

const express = require('express');
const middlewares = require('../middlewares');
const controllers = require('../controllers');

const router = express.Router();

router.get('/bailProprietaireId/:userId', controllers.bail.getBailProprietaireId);
router.get('/bailLocataireId/:userId', controllers.bail.getBailLocataireId);
router.get('/:bailId', controllers.bail.getBailAgent);
router.post('/', middlewares.bail.signedBailControl, controllers.bail.postBail);
router.get('/', controllers.bail.getBails);
//router.get('/:bailId', controllers.bail.getBail);
router.get('/:bailId', controllers.bail.getBailAgent);


module.exports = router;