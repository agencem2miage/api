/*
 Navicat Premium Data Transfer

 Source Server         : Bénélove
 Source Server Type    : MySQL
 Source Server Version : 100410
 Source Host           : localhost:3306
 Source Schema         : agence_zemmari

 Target Server Type    : MySQL
 Target Server Version : 100410
 File Encoding         : 65001

 Date: 27/05/2021 16:05:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for adresse
-- ----------------------------
DROP TABLE IF EXISTS `adresse`;
CREATE TABLE `adresse`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `rue` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `complement` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `codePostal` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ville` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pays` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bail
-- ----------------------------
DROP TABLE IF EXISTS `bail`;
CREATE TABLE `bail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datePriseEffet` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `dureeBail` int(2) NOT NULL,
  `dureeBailReduite` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `justificationDureeBailReduite` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `periodicite` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `loyerHorsCharges` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `chargesRecuperables` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `isLoyerReference` tinyint(1) NOT NULL,
  `isMontantMaximumAnnuel` tinyint(1) NOT NULL,
  `loyerReference` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `loyerAncienLocataire` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `modaliteReglementChargesRecuperables` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `montantChargesRecuperables` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `souscriptionAssuranceParBailleur` tinyint(1) NOT NULL,
  `periodicitePaiementLoyer` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `echeancePaiement` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `datePeriodePaiement` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `montantPremiereEcheance` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `montantRecuperableAssurance` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `montantRecuperableAssuranceDouzieme` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `montantGaranties` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `usageLocaux` int(2) NOT NULL,
  `signatureProprietaire` tinyint(1) NOT NULL,
  `signatureLocataire` tinyint(1) NOT NULL,
  `pdf` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idBien` int(11) NOT NULL,
  `idLocataire` int(11) NOT NULL,
  `assuranceProprietaire` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `edlEntree` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `edlSortie` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idBien`(`idBien`) USING BTREE,
  INDEX `idLocataire`(`idLocataire`) USING BTREE,
  INDEX `bail_ibfk_3`(`usageLocaux`) USING BTREE,
  CONSTRAINT `bail_ibfk_1` FOREIGN KEY (`idBien`) REFERENCES `bien` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bail_ibfk_2` FOREIGN KEY (`idLocataire`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bail_ibfk_3` FOREIGN KEY (`usageLocaux`) REFERENCES `usage_locaux` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for bien
-- ----------------------------
DROP TABLE IF EXISTS `bien`;
CREATE TABLE `bien`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prixLocation` int(11) NOT NULL,
  `anneeConstruction` int(11) NOT NULL,
  `surfaceHabitable` int(11) NOT NULL,
  `surfaceTerrain` int(11) NOT NULL,
  `surfaceBalcon` int(11) NOT NULL,
  `equipement` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `detailsPiece` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ges` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `classeEnergie` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `meuble` tinyint(1) NOT NULL,
  `garageVelo` tinyint(1) NOT NULL,
  `garageVoiture` tinyint(1) NOT NULL,
  `espacesVerts` tinyint(1) NOT NULL,
  `localPoubelle` tinyint(1) NOT NULL,
  `ascenseur` tinyint(1) NOT NULL,
  `aireJeux` tinyint(1) NOT NULL,
  `gardiennage` tinyint(1) NOT NULL,
  `laverie` tinyint(1) NOT NULL,
  `idRegimeJuridique` int(11) NOT NULL,
  `idTypeContrat` int(11) NOT NULL,
  `idTypeBien` int(11) NOT NULL,
  `idProprietaire` int(11) NOT NULL,
  `idAdresse` int(11) NOT NULL,
  `assuranceLocataire` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idTypeContrat`(`idTypeContrat`) USING BTREE,
  INDEX `idTypeBien`(`idTypeBien`) USING BTREE,
  INDEX `idProprietaire`(`idProprietaire`) USING BTREE,
  INDEX `idAdresse`(`idAdresse`) USING BTREE,
  INDEX `idRegimeJuridique`(`idRegimeJuridique`) USING BTREE,
  CONSTRAINT `bien_ibfk_1` FOREIGN KEY (`idTypeContrat`) REFERENCES `type_contrat` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bien_ibfk_2` FOREIGN KEY (`idTypeBien`) REFERENCES `type_bien` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bien_ibfk_3` FOREIGN KEY (`idProprietaire`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bien_ibfk_4` FOREIGN KEY (`idAdresse`) REFERENCES `adresse` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `bien_ibfk_5` FOREIGN KEY (`idRegimeJuridique`) REFERENCES `regime_juridique` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for duree_bail
-- ----------------------------
DROP TABLE IF EXISTS `duree_bail`;
CREATE TABLE `duree_bail`  (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idBien` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idBien`(`idBien`) USING BTREE,
  CONSTRAINT `image_ibfk_1` FOREIGN KEY (`idBien`) REFERENCES `bien` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for majoration
-- ----------------------------
DROP TABLE IF EXISTS `majoration`;
CREATE TABLE `majoration`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idBail` int(11) NOT NULL,
  `natureTravaux` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `delai` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `modalite` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `montant` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idBien`(`idBail`) USING BTREE,
  CONSTRAINT `maj_fk_bail` FOREIGN KEY (`idBail`) REFERENCES `bail` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for piece_bien
-- ----------------------------
DROP TABLE IF EXISTS `piece_bien`;
CREATE TABLE `piece_bien`  (
  `idPieceBien` int(11) NOT NULL AUTO_INCREMENT,
  `idTypePiece` int(11) NOT NULL,
  `idBien` int(11) NOT NULL,
  PRIMARY KEY (`idPieceBien`) USING BTREE,
  INDEX `idBien`(`idBien`) USING BTREE,
  INDEX `idTypePiece`(`idTypePiece`) USING BTREE,
  CONSTRAINT `piece_bien_ibfk_1` FOREIGN KEY (`idBien`) REFERENCES `bien` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `piece_bien_ibfk_2` FOREIGN KEY (`idTypePiece`) REFERENCES `type_piece` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for regime_juridique
-- ----------------------------
DROP TABLE IF EXISTS `regime_juridique`;
CREATE TABLE `regime_juridique`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for travaux
-- ----------------------------
DROP TABLE IF EXISTS `travaux`;
CREATE TABLE `travaux`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idBien` int(11) NOT NULL,
  `nature` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `montant` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idBien`(`idBien`) USING BTREE,
  CONSTRAINT `travaux_ibfk_1` FOREIGN KEY (`idBien`) REFERENCES `bien` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for type_bien
-- ----------------------------
DROP TABLE IF EXISTS `type_bien`;
CREATE TABLE `type_bien`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for type_contrat
-- ----------------------------
DROP TABLE IF EXISTS `type_contrat`;
CREATE TABLE `type_contrat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for type_piece
-- ----------------------------
DROP TABLE IF EXISTS `type_piece`;
CREATE TABLE `type_piece`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for usage_locaux
-- ----------------------------
DROP TABLE IF EXISTS `usage_locaux`;
CREATE TABLE `usage_locaux`  (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `civilite` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `prenom` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nom` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `mail` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tel` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `dateNaissance` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `lieuNaissance` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `mdp` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `idAdresse` int(11) NOT NULL,
  `idRole` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idAdresse`(`idAdresse`) USING BTREE,
  INDEX `idRole`(`idRole`) USING BTREE,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`idAdresse`) REFERENCES `adresse` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`idRole`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- View structure for bail_view
-- ----------------------------
DROP VIEW IF EXISTS `bail_view`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `bail_view` AS select `bail`.`id` AS `BAIL_id`,`bail`.`datePriseEffet` AS `BAIL_date_prise_effet`,`bail`.`dureeBail` AS `BAIL_duree`,`bail`.`dureeBailReduite` AS `BAIL_duree_reduite`,`bail`.`justificationDureeBailReduite` AS `BAIL_justification_duree_reduite`,`bail`.`periodicite` AS `BAIL_periodicite`,`bail`.`loyerHorsCharges` AS `BAIL_loyer_hc`,`bail`.`chargesRecuperables` AS `BAIL_charges_recuperables`,`bail`.`isLoyerReference` AS `BAIL_is_loyer_reference`,`bail`.`isMontantMaximumAnnuel` AS `BAIL_is_montant_maximum_annuel`,`bail`.`loyerReference` AS `BAIL_loyer_reference`,`bail`.`loyerAncienLocataire` AS `BAIL_loyer_ancien_locataire`,`bail`.`modaliteReglementChargesRecuperables` AS `BAIL_modalite_reglement_charges_recuperables`,`bail`.`montantChargesRecuperables` AS `BAIL_montant_charges_recuperables`,`bail`.`souscriptionAssuranceParBailleur` AS `BAIL_souscription_assurance_par_bailleur`,`bail`.`periodicitePaiementLoyer` AS `BAIL_periodicite_paiement_loyer`,`bail`.`echeancePaiement` AS `BAIL_echeance_paiement`,`bail`.`datePeriodePaiement` AS `BAIL_date_periode_paiement`,`bail`.`montantPremiereEcheance` AS `BAIL_montant_premiere_echeance`,`bail`.`montantRecuperableAssurance` AS `BAIL_montant_recuperable_assurance`,`bail`.`montantRecuperableAssuranceDouzieme` AS `BAIL_montant_recuperable_assurance_douzieme`,`bail`.`montantGaranties` AS `BAIL_montant_garanties`,`bail`.`usageLocaux` AS `BAIL_usage_locaux`,`bail`.`signatureProprietaire` AS `BAIL_signature_proprietaire`,`bail`.`signatureLocataire` AS `BAIL_signature_locataire`,`bail`.`pdf` AS `BAIL_pdf`,`bail`.`idBien` AS `BAIL_id_bien`,`bail`.`idLocataire` AS `BAIL_id_locataire`,`bail`.`assuranceProprietaire` AS `BAIL_assurance_proprietaire`,`bail`.`edlEntree` AS `BAIL_edl_entree`,`bail`.`edlSortie` AS `BAIL_edl_sortie`,`usage_locaux`.`label` AS `USAGE_LOCAUX_label`,`duree_bail`.`label` AS `DUREE_BAIL_label` from ((`bail` join `usage_locaux` on(`bail`.`usageLocaux` = `usage_locaux`.`id`)) join `duree_bail` on(`bail`.`dureeBail` = `duree_bail`.`id`));

-- ----------------------------
-- View structure for bien_view
-- ----------------------------
DROP VIEW IF EXISTS `bien_view`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `bien_view` AS select `bien`.`id` AS `BIEN_ID`,`bien`.`prixLocation` AS `BIEN_PRIX_LOCATION`,`bien`.`anneeConstruction` AS `ANNEE_CONSTRUCTION`,`bien`.`surfaceHabitable` AS `SURFACE_HABITABLE`,`bien`.`surfaceTerrain` AS `SURFACE_TERRAIN`,`bien`.`surfaceBalcon` AS `SURFACE_BALCON`,(select count(0) from (`piece_bien` `p` join `bien` `b` on(`b`.`id` = `p`.`idBien`)) where `p`.`idBien` = `bien`.`id` group by `b`.`id`) AS `NB_PIECE`,(select `i`.`label` from `image` `i` where `i`.`idBien` = `bien`.`id` order by `i`.`id` limit 1) AS `PREMIERE_IMAGE`,(select count(0) from (`piece_bien` `p` join `bien` `b` on(`b`.`id` = `p`.`idBien`)) where `p`.`idBien` = `bien`.`id` and `p`.`idTypePiece` = 6 group by `b`.`id`) AS `NB_CHAMBRE`,(select count(0) from (`piece_bien` `p` join `bien` `b` on(`b`.`id` = `p`.`idBien`)) where `p`.`idBien` = `bien`.`id` and (`p`.`idTypePiece` = 5 or `p`.`idTypePiece` = 2) group by `b`.`id`) AS `NB_SALLE_DE_BAIN`,`bien`.`equipement` AS `EQUIPEMENT`,`bien`.`detailsPiece` AS `DETAILS_PIECE`,`bien`.`description` AS `DESCRIPTION`,`bien`.`ges` AS `GES`,`bien`.`classeEnergie` AS `CLASSE_ENERGIE`,`bien`.`meuble` AS `MEUBLE`,`bien`.`garageVelo` AS `GARAGE_VELO`,`bien`.`garageVoiture` AS `GARAGE_VOITURE`,`bien`.`espacesVerts` AS `ESPACES_VERTS`,`bien`.`localPoubelle` AS `LOCAL_POUBELLE`,`bien`.`ascenseur` AS `ASCENSEUR`,`bien`.`aireJeux` AS `AIRE_JEUX`,`bien`.`gardiennage` AS `GARDIENNAGE`,`bien`.`laverie` AS `LAVERIE`,`bien`.`idRegimeJuridique` AS `ID_REGIME_JURIDIQUE`,`regime_juridique`.`label` AS `REGIME_JURIDIQUE_LABEL`,`bien`.`idTypeContrat` AS `ID_TYPE_CONTRAT`,`type_contrat`.`label` AS `TYPE_CONTRAT_LABEL`,`bien`.`idTypeBien` AS `ID_TYPE_BIEN`,`type_bien`.`label` AS `TYPE_BIEN_LABEL`,`bien`.`idProprietaire` AS `ID_PROPRIETAIRE`,`user`.`civilite` AS `USER_CIVILITE`,`user`.`prenom` AS `USER_PRENOM`,`user`.`nom` AS `USER_NOM`,`user`.`mail` AS `USER_MAIL`,`user`.`tel` AS `USER_TEL`,`user`.`dateNaissance` AS `USER_DATE_NAISSANCE`,`user`.`lieuNaissance` AS `USER_LIEU_NAISSANCE`,`user`.`idAdresse` AS `USER_ID_ADRESSE`,`bien`.`idAdresse` AS `BIEN_ID_ADRESSE`,`adresse_bien`.`numero` AS `ADRESSE_BIEN_NUMERO`,`adresse_bien`.`rue` AS `ADRESSE_BIEN_RUE`,`adresse_bien`.`complement` AS `ADRESSE_BIEN_COMPLEMENT`,`adresse_bien`.`codePostal` AS `ADRESSE_BIEN_CODE_POSTAL`,`adresse_bien`.`ville` AS `ADRESSE_BIEN_VILLE`,`adresse_bien`.`pays` AS `ADRESSE_BIEN_PAYS`,`adresse_user`.`numero` AS `ADRESSE_USER_NUMERO`,`adresse_user`.`rue` AS `ADRESSE_USER_RUE`,`adresse_user`.`complement` AS `ADRESSE_USER_COMPLEMENT`,`adresse_user`.`codePostal` AS `ADRESSE_USER_CODE_POSTAL`,`adresse_user`.`ville` AS `ADRESSE_USER_VILLE`,`adresse_user`.`pays` AS `ADRESSE_USER_PAYS`,`bail`.`id` AS `BAIL_ID`,`bail`.`idLocataire` AS `LOCATAIRE_ID`,`locataire`.`civilite` AS `LOCATAIRE_CIVILITE`,`locataire`.`prenom` AS `LOCATAIRE_PRENOM`,`locataire`.`nom` AS `LOCATAIRE_NOM`,`locataire`.`mail` AS `LOCATAIRE_MAIL`,`locataire`.`tel` AS `LOCATAIRE_TEL`,`locataire`.`idAdresse` AS `LOCATAIRE_ID_ADRESSE`,`bien`.`assuranceLocataire` AS `ASSURANCE_LOCATAIRE` from ((((((((`bien` join `regime_juridique` on(`bien`.`idRegimeJuridique` = `regime_juridique`.`id`)) join `type_contrat` on(`bien`.`idTypeContrat` = `type_contrat`.`id`)) join `type_bien` on(`bien`.`idTypeBien` = `type_bien`.`id`)) join `user` on(`bien`.`idProprietaire` = `user`.`id`)) join `adresse` `adresse_bien` on(`bien`.`idAdresse` = `adresse_bien`.`id`)) join `adresse` `adresse_user` on(`user`.`id` = `adresse_user`.`id`)) left join `bail` on(`bien`.`id` = `bail`.`idBien`)) left join `user` `locataire` on(`bail`.`idLocataire` = `locataire`.`id`)) group by `bien`.`id`;

-- ----------------------------
-- View structure for user_view
-- ----------------------------
DROP VIEW IF EXISTS `user_view`;
CREATE ALGORITHM = UNDEFINED DEFINER = `root`@`localhost` SQL SECURITY DEFINER VIEW `user_view` AS select `user`.`id` AS `USER_id`,`user`.`civilite` AS `USER_civilite`,`user`.`prenom` AS `USER_prenom`,`user`.`nom` AS `USER_nom`,`user`.`mail` AS `USER_mail`,`user`.`tel` AS `USER_tel`,`user`.`dateNaissance` AS `USER_dateNaissance`,`user`.`lieuNaissance` AS `USER_lieuNaissance`,`user`.`mdp` AS `USER_mdp`,`role`.`id` AS `ROLE_id`,`role`.`label` AS `ROLE_label`,`adresse`.`id` AS `ADRESSE_id`,`adresse`.`numero` AS `ADRESSE_numero`,`adresse`.`rue` AS `ADRESSE_rue`,`adresse`.`complement` AS `ADRESSE_complement`,`adresse`.`codePostal` AS `ADRESSE_codePostal`,`adresse`.`ville` AS `ADRESSE_ville`,`adresse`.`pays` AS `ADRESSE_pays` from ((`user` join `role`) join `adresse`) where `user`.`idRole` = `role`.`id` and `user`.`idAdresse` = `adresse`.`id`;

SET FOREIGN_KEY_CHECKS = 1;
